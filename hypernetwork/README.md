# Code for CPG-RBF-Hyper network
This repository provides an implementation of the paper:

Hybrid learning mechanisms under a neural control network for various walking speed generation of a quadruped robot

Here we present the CPG-RBF-Hyper control network to integrate probability-based black-box optimization (*PI^{BB}*) and supervised learning for a robot's motor pattern generation under various walking speeds. Firstly, the CPG driven RBF network acting as a complex motor pattern generator was trained to learn multiple motor patterns (determined by policies) for different speeds using *PI^{BB}*. Secondly, the Hyper network acting as a task/behaviour-to-control parameter mapping was trained using supervised learning. Finally, when a user-defined robot walking frequency is given, the Hyper network generate the corresponding policy for the CPG-RBF network. The result is a verstatile locomotion controller which can generate corresponding motor patterns for varing speeds. 

## Content:
* [Prerequisites](#Prerequisites)
* [Code overview](#code-overview)
* [Install]()
* [Run the]()
*

# Prerequisites
* Intel® Core™ i9-9900K CPU @ 3.60GHz × 16
* GeForce RTX 2080
* Ubuntu 18.04 LTS
* CoppeliaSim V4.1.0
* Python 2.7 or above
* PyTorch
* ROS melodic

# Code overview
The following explains the content of the  main directories:
* **data**: Contains data from running the simulation and PI^BB algorithm (both data used in the result section (in the storage directory)). Moreover, it contains the learned weight sets (as `.json` files) for the CPG-RBF controller. For a more detailed explaination see the README.file in the data directory.
* **Hyper_network**: Contains all the code necessary for the Hyper network.  The Hyper network's output is in the `hn_output_policy`, and the Hyper network's trainging sets are collected in the `hn_training_sample`.  
* **interface**: Contains `.lua` files for interfacing with and setting up the simulation. It also contains the `build_dir` for `cmake`
* **neural_controllers**: Contains all the code necessary for the CPG-RBF control network 
* **machine_learning**: Contains all the code necessary for the PI^BB learning algorithm as well as bash scripts for running the simulation.
* **simulations**: Contains coppeliaSim simulation environments (including the Laikago robot).
* **utils**: Contains additional utilities needed by the controller implementary and simulation inferface.




# Install
First, for CPG-RBF NETWORK to learn policies using PI^BB, we need to setup the simulation:

1. Download coppeliaSim EDU from the downloads page

2. Clone this repository to your local machine

`git clone ` 

3. Extract the downloaded .zip file into a directory as many times as you need "simulation workers"(i.e., the number of simulations running in parallel. We used four workers in all our experinments).

4. Rename the extracted CoppeliaSim directories as: `VREP1`, `VREP2`, `VREP3`,`VREP4`, etc.

5. In `remoteApiConnections.txt` in each of the `VREP#` directories, change `portIndex1_port` so that `VREP1` has `19997`, `VREP2` has `19996`, `VREP3` has `19995`,`VREP4` has `19994`, etc.

6. Copy `libv_repExtRosInterface.so` into each of the walker directories from the utils directory.

`cp $FRAMEWORK_PATH/CPG-RBFN-Hyper-framework/utils/libv_repExtRosInterface.so $VREP_WORKER_PATH/VREP1/`

7. Install the required pyhton libraries *(matplotlib, jupyter, drawnow, and numpy)*

`cd $FRAMEWORK_PATH/CPG-RBFN-Hyper-framework/`
`sudo apt install python3-pip`
`pip3 install -r requirements.txt`

8. Install lIB GSL.
`sudo apt-get install libgsl-dev`

The neural controllers use ROS to communicate with coppeliaSim, Therefore, make sure that `ros-xxx-desktop-full` _(tested on melodic)_ is installed.

# Run the CPG-RBF network to learn policies

The following will show how to learn a policy for CPG-RBF network.
1. Start a ROS core

`roscore`

2. Start the simulation workers. In the example, we will use four workers.

`cd $VREP_WORKER_PATH/VREP1/`

`./coppeliaSim.sh $FRAMEWORK_PATH/CPG-RBFN-Hyper-framework/simulations/MORF_base_behavior.ttt`

`cd $VREP_WORKER_PATH/VREP2/`

`./coppeliaSim.sh $FRAMEWORK_PATH/CPG-RBFN-Hyper-framework/simulations/MORF_base_behavior.ttt`

`cd $VREP_WORKER_PATH/VREP3/`

`./coppeliaSim.sh $FRAMEWORK_PATH/CPG-RBFN-Hyper-framework/simulations/MORF_base_behavior.ttt`

`cd $VREP_WORKER_PATH/VREP4/`

`./coppeliaSim.sh $FRAMEWORK_PATH/CPG-RBFN-Hyper-framework/simulations/MORF_base_behavior.ttt`

3. In `$FRAMEWORK_PATH/CPG-RBFN-Hyper-framework/neural_controllers/laikago/sim/neurtronController.cpp` set the walking frequency. The walking frequencies we used in our work are `0.06*PI`, `0.08*PI`, `0.10*PI`, `0.12*PI`, `0.14*PI` and `0.16*PI`.

`phi =  $VALUE * M_PI ; `

4. Build the locomotion controller.

`cd $FRAMEWORK_PATH/CPG-RBFN-Hyper-framework/interfaces/morf/sim/build_dir`

`rm CMakeCache.txt`

`cmake .`

`make`


4. In `$FRAMEWORK_PATH/CPG-RBFN-Hyper-framework/machine_learning/RL_encoding.py` set the initial weights. When using the Gaussian noise as initial weights for CPG-RBF network, we choose:

`init_parameter_set = set_BC + set_CF + set_FT`.

When using the learned policy as initial weights, we choose:

```
with open ('$FRAMEWORK_PATH/CPG-RBFN-Hyper-framework/**json') as json_file:

    data = json.load(json_file)

    init_parameter_set = data[ParameterSet]
```
    

5. In `$FRAMEWORK_PATH/CPG-RBFN-Hyper-framework/machine_learning/RL_master.py` set the variance of exploration noise. When using the Gaussian noise as initial weights for CPG-RBF network, we choose variance is `0.015`. However, when using the learned policy as initial weights, we choose variance is `0.01`.

`variance = 0.015 or 0.01`



6. Start the learning algorithm.

`./RL_repeater.sh -t 1 -e indirect -r LAIKAGO`

7.  To get training samples for Hyper network, we train policy for `0.06pi` firstly with initial weights as Gaussian noise. Then, we use the learned policy of `0.06pi` as the initial weights to train policy for `0.08pi`. Similarly, the learned policy of `0.08pi` acts as the initial policy for learning `0.1pi`, `0.1pi` for learning `0.12pi`, `0.12pi` for learning `0.14pi`, `0.14pi` for learning `0.16pi`. We repeated this process 5 times, the learned policies are collected in the `$FRAMEWORK_PATH/CPG-RBFN-Hyper-framework/Hyper_network/hn_training_sample` 

# Train the Hyper network

# Run the learned CPG-RBF-Hyper network




