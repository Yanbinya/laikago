cmake_minimum_required(VERSION 2.8.3)
project(laikago_controller)

set (CMAKE_CXX_STANDARD 11)

SET (GOROBOTS "../../../..")
SET (CONTROLLER "${GOROBOTS}/neural_controllers/laikago")
SET (UTILS "${GOROBOTS}/utils")
SET (RAPIDJSON "${CONTROLLER}/rapidjson")

find_package(catkin REQUIRED)
find_package(catkin REQUIRED COMPONENTS std_msgs geometry_msgs roscpp sensor_msgs)

include_directories("${catkin_INCLUDE_DIRS}"
        "${GOROBOTS}"
        "${UTILS}"
		"${UTILS}/vrepRemoteAPI"
		"${RAPIDJSON}")

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

add_definitions(-DDO_NOT_USE_SHARED_MEMORY)
add_definitions(-DMAX_EXT_API_CONNECTIONS=255)
add_definitions(-DNON_MATLAB_PARSING)

add_executable(
        laikago_controller
        ${CONTROLLER}/sim/main_hexapod_controller
        ${CONTROLLER}/sim/simRosClass
        ${CONTROLLER}/sim/modularController
        ${CONTROLLER}/sim/neutronMotorDefinition.h
        ${CONTROLLER}/sim/dualIntegralLearner
        ${CONTROLLER}/sim/neutronController
        ${CONTROLLER}/sim/postProcessing
        ${CONTROLLER}/sim/rbfcpg
        ${CONTROLLER}/sim/rbfn
        ${UTILS}/ann-framework/ann
        ${UTILS}/ann-framework/neuron
        ${UTILS}/ann-framework/synapse
        ${UTILS}/ann-library/psn
        ${UTILS}/ann-library/vrn
        ${UTILS}/ann-library/pmn
        ${UTILS}/ann-library/so2cpg
        ${UTILS}/ann-library/adaptiveso2cpgsynplas
        ${UTILS}/ann-library/extendedso2cpg
        ${UTILS}/ann-library/pcpg
        ${UTILS}/delayline
        ${UTILS}/interpolator2d
		${UTILS}/vrepRemoteAPI/extApiPlatform
		${UTILS}/vrepRemoteAPI/extApiInternal
		${UTILS}/vrepRemoteAPI/extApi
		${UTILS}/vrepRemoteAPI/simConst
		${UTILS}/environment)

target_link_libraries(laikago_controller ${catkin_LIBRARIES})
add_dependencies(laikago_controller ${catkin_EXPORTED_TARGETS})
