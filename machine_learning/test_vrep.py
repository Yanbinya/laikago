#!/usr/bin/env python

import sim
import sys
import time

def main(argv):
    # Refer to http://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctionsPython.htm#simxStart

    sim.simxFinish(-1) # just in case, close all opened connections
    clientID = sim.simxStart('127.0.0.1', 19997, True, True, 5000, 5)

    if clientID == -1:
        print('Failed connecting to remote API server')
    else:
        print('Connected to remote API client')

        count = 0
        dt = 0.0167
        sim.simxSynchronous(clientID, True)
        sim.simxStartSimulation(clientID, sim.simx_opmode_blocking)

        # run for 1 simulated second
        while count < 10:
            sim.simxSynchronousTrigger(clientID) # Simulation is triggered in C++

            # sim.simxGetPingTime(clientID)
            # count = sim.simxGetLastCmdTime(clientID)/1000

            _, count = sim.simxGetFloatSignal(clientID, "mySimulationTime", sim.simx_opmode_blocking)

            print(count)

        sim.simxStopSimulation(clientID, sim.simx_opmode_blocking)
        sim.simxGetPingTime(clientID)
        sim.simxFinish(clientID)


if __name__ == '__main__':
    main(sys.argv[1:])
